const express = require("express");
const app = express();
const env = require("dotenv").config();
const routes = require("./src/routes");
const { initializer } = require("./helpers/index");
initializer({});

app.use(express.json());

app.use(routes);
app.listen(process.env.PORT || 5000, () =>
  console.log(`Server running on port ${process.env.PORT || 5000} 🔥`),
);

module.exports = app;
