const router = require("express").Router();

const { users, addUser } = require("../../controllers");

router.get("/", users);

router.post("/add", addUser);

module.exports = router;
