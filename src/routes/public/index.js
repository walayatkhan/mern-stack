const router = require("express").Router();
const loginRouter = require("./login.route");

const publicRoutes = [
  {
    path: "/user",
    route: loginRouter,
  },
];

module.exports = publicRoutes;
