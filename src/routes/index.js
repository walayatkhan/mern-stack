const router = require("express").Router();

const { authrized } = require("../middlewares");
const publicRoutes = require("../routes/public");

publicRoutes.forEach((route) => {
  router.use(route.path, authrized, route.route);
});

module.exports = router;
