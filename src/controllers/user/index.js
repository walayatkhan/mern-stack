const { getAll, addOne } = require("../../models");

let users = async (req, res) => {
  let result = await getAll();
  res.send(result);
};

let addUser = async (req, res) => {
  try {
    let result = await addOne(req.body);

    res.send(result);
  } catch (e) {
    res.send({ message: e, status: 500 });
  }
};

module.exports = { users, addUser };
