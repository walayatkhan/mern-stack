const { users, addUser } = require("./user");

module.exports = { users, addUser };
