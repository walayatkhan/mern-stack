const { User } = require("../../schemas");

let getAll = async (req, res) => {
  let data = await User.find();
  return {
    data: data,
    status: 200,
  };
};

let addOne = async (req, res) => {
  try {
    console.log("after save", req);
    let result = await User(req).save();
  } catch (e) {
    return { message: "error", data: e, status: 422 };
  }
};

module.exports = { getAll, addOne };
