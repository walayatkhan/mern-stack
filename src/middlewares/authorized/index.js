const authrized = (request, response, next) => {
    try{
        if(!request.headers['x-api-key'] || request.headers['x-api-key'] !== process.env.API_KEY)
        {
            return {message: 'Access Denied', body: response, status: 422 }
        }
        next();
    }catch(e){
        return {message: 'an error occured', error: e}
    }
}

module.exports = authrized;