const mongoose = require("mongoose");

let connect = () => {
  mongoose
    .connect(process.env.DB_URL)
    .catch((error) => console.log("Mongoose connection issue.", error));

  mongoose.connection.on("connected", () =>
    console.log("Mongoose connected successfully."),
  );
  mongoose.connection.on("error", (error) =>
    console.log("An error occured while connecting to the db.", error),
  );
  mongoose.connection.on("disconnected", () =>
    console.log("Mongoose Disconnected."),
  );
};
module.exports = connect;
