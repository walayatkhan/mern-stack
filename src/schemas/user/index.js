const mongoose = require("mongoose");
const UserSchema = new mongoose.Schema({
  first_name: { type: String },
  last_name: { type: String },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    required: "Email Address is required",
  },
  password: { type: String },
  remember_me: { type: Boolean },
});

module.exports = mongoose.model("User", UserSchema);
