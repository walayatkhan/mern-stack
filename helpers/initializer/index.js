let connect = require("../../src/config/connection");

const initializer = (request) => {
  connect();
};

module.exports = initializer;
